var x = 6, y = 14, z = 4 
document.write("x += y - x++ * z = ", x += y - x++ * z, "<br>")
/*
6 += 14 - 6 * 4
6 += 14 - 24
6 += -10
-4
*/

var x = 6, y = 14, z = 4 
document.write("z = --x - y * 5 = ", z = --x - y * 5, "<br>")
/*
z = 5 - 14 * 5
z = 5 - 70
z = -65
*/

var x = 6, y = 14, z = 4 
document.write("y /= x + 5 % z = ", y /= x + 5 % z, "<br>")
/*
14 /= 6 + 5 % 4
14 /= 6 + 1
14 /= 7
2
*/

var x = 6, y = 14, z = 4 
document.write("z - x++ + y * 5 = ", z - x++ + y * 5, "<br>")
/*
4 - 6 + 14 * 5
-2 + 70
68
*/

var x = 6, y = 14, z = 4 
document.write("x = y - x++ * z = ", x = y - x++ * z, "<br>")
/*
x = 14 - 6 * 4
x = 14 - 24
x = -10
*/